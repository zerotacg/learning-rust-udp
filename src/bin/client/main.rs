use rust_udp::DEFAULT_ADDRESS;
use std::env;
use std::net::{Ipv4Addr, UdpSocket};

fn main() -> std::io::Result<()> {
    {
        let args: Vec<String> = env::args().collect();
        let default_address = String::from(DEFAULT_ADDRESS);
        let address = args.get(2).unwrap_or(&default_address);
        let message = args.get(1).expect("no message given");
        let socket = UdpSocket::bind((Ipv4Addr::UNSPECIFIED, 0)).expect("couldn't bind to address");

        socket
            .send_to(message.as_ref(), address)
            .expect("couldn't send data");
    }
    Ok(())
}
