use std::env;
use std::net::UdpSocket;

use rust_udp::DEFAULT_ADDRESS;

fn main() {
    let args: Vec<String> = env::args().collect();
    let default_address = String::from(DEFAULT_ADDRESS);
    let address = args.get(1).unwrap_or(&default_address);
    println!("starting server on: {}", address);
    let socket = UdpSocket::bind(address).expect("couldn't bind to address");

    let mut buf = [0; 10];
    loop {
        let (amt, src) = socket.recv_from(&mut buf).expect("Didn't receive data");

        let message = String::from_utf8_lossy(&mut buf[..amt]);
        println!("msg from {}: {}", src, message);
    }
}
